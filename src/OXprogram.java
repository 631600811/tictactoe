
import java.util.Scanner;

public class OXprogram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {

        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn" + " " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.print("Please input row, col:");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTalbe()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            count++;
            if(checkDraw()){
                finish = true;
                showDraw();
                return;
            }
            switchPlayer();
        }
    }

    private static void showWin() {
        showTable();
        System.out.println(">>> " + currentPlayer + " Win <<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTalbe() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizon()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizon() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if ((table[i][i]) != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if ((table[i][2-i]) != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
       if(count == 9){
           return true;
       }
       return false;
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>> DRAW <<<");
    }

}
